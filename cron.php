<?php

require_once('Nike/Runner.php');

$ping = file_get_contents('https://www.easyhuzl.com/api/ping');
if($ping) {
	$ping_json = json_decode($ping, true);

	if($ping_json['success']) {

		$action  = $ping_json['action'];
		$account = $ping_json['account'];
		$other   = $ping_json['other'];

		if (!empty($account['data_dir'])) {
			// todo: unpack and load correctly.
		}

		$runner = new Runner($action, $account, $other);
		$runner->main();
	} else {
		echo 'Failure:';

		print_r($ping_json);
	}
} else {
	echo 'Nothing new...';
}