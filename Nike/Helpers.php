<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;

class Helpers
{
	public function waitUntilShown($driver, $selector, $type, $ms, $max_count = 10)
	{
		$element = false;
		$count = 0;

		while(!$element && $count < $max_count) {
			try {
				if($type == 'css') {
					$selection = WebDriverBy::cssSelector($selector);
				}

				$element = $driver->findElement($selection);
			} catch (NoSuchElementException $e) {
				$count++;

				logm('Element ('.$selector.' via '.$type.') not shown waiting '. $ms .'ms ('.$count.'/'.$max_count.')');

				usleep($ms * 1000);
			}
		}

		if(!$element) {
			return false;
		}

		return $element;
	}
}