<?php

namespace Controllers;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class Login
{
	/** @var RemoteWebDriver */
	private $driver;
	private $log;
	private $helpers;

	function __construct($driver, $log, $helpers)
	{
		$this->driver = $driver;
		$this->log = $log;
		$this->helpers = $helpers;
	}

	public function isLoggedIn()
	{
		logm('Checking if logged in.');

		$nike_fullname = $join_or_login_button = false;

		sleep(5); // For some reason it requires some time to ~load~ maybe in the future we can check to see if all network requests are complete?

		while(!$nike_fullname && !$join_or_login_button) {
			$join_or_login_button_elements = $this->driver->findElements(WebDriverBy::cssSelector('[data-qa="top-nav-join-or-login-button"]'));
			$nike_fullname_elements = $this->driver->findElements(WebDriverBy::className('test-name'));
			$my_account_elements = $this->driver->findElements(WebDriverBy::cssSelector('[data-qa="user-name"]')); // Sometimes just "My Account" shows
			if(count($my_account_elements) > 0) {
				logm('Nike is logged in! (we see my account)');

				return true;
			} else if(count($nike_fullname_elements) > 0) {
				$nike_fullname = $nike_fullname_elements[0];
			} else if(count($join_or_login_button_elements) > 0) {
				$join_or_login_button = true; // We don't actually care for the element at this point. Maybe in the future we click on it instead.
			} else {
				logm('Nike full name (and login button) has not loaded, waiting 4s before re-checking');

				sleep(4);
			}
		}

		if($join_or_login_button) {
			return false;
		}

		logm('Nike is logged in! Full name has loaded as ' . $nike_fullname->getText());

		return $nike_fullname->getText() === $this->account['fullname']; // What do we do if this is false and we're logged into the wrong account?
	}

	public function login($username, $password)
	{
		$current_url = $this->driver->getCurrentURL();

		logm('Saving current url: ' .$current_url);

		logm('Loading login page to login');

		$this->driver->get('https://www.nike.com/login');

		$username_field = $this->helpers->waitUntilShown($this->driver, '[name="emailAddress"]', 'css', 3000);
		$password_field = $this->driver->findElement(WebDriverBy::cssSelector('[name="password"]'));
		$sign_in_button = $this->driver->findElement(WebDriverBy::cssSelector('[value="SIGN IN"]'));

		if($username_field) {
			logm('Entering username: ' . $username);

			$username_field->clear();
			$username_field->sendKeys($username);
		}

		if($password_field) {
			logm('Entering password: ' . $password);

			$password_field->clear();
			$password_field->sendKeys($password);
		}

		if($sign_in_button) {
			logm('Clicking sign in button and then sleeping for 20s');

			$sign_in_button->click();
		}

		// todo: check if error

		sleep(20); // todo make this wait until logged in confirmed

		logm('Navigating to previous url.');
		$this->driver->get($current_url);

		sleep(2);

		//$this->helpers->waitUntilShown($this->driver, 'test-name', 'css', 3000);
	}
}
