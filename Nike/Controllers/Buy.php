<?php

namespace Controllers;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverPlatform;
use Facebook\WebDriver\Remote\WebDriverBrowserType;

require_once('vendor/autoload.php');

class Buy
{
    /** @var RemoteWebDriver */
	private $driver;
	private $log;

	private $account;
	private $other;

	private $has_reloaded = false;

    function __construct($driver, $log, $helpers, $account, $other)
    {
        $this->driver = $driver;
		$this->log = $log;
		$this->helpers = $helpers;

		$this->account = $account;
		$this->other = $other;

		$this->login = new Login($driver, $log, $helpers);
    }

    public function main()
    {
        logm("Loading Nike page");

        $this->driver->get($this->other['drop_url']);

		if(!$this->login->isLoggedIn()) {
			$this->login->login($this->account['username'], $this->account['password']);
		}


		// Logged in, now make see if the shoes are dropped...

		// class: buyable-full-width
		// todo: for when div.gettext = Sold Out or notify me
		// data-qa="spinner-img"

		while(!$this->hasDropped()) {
			logm('Shoe has not dropped yet. Sleeping for 5s and then rechecking.');

			sleep(5);
		}

		logm('Shoe is live! Looking for available sizes.');

		// todo: stale element? https://i.imgur.com/l5qvJxI.jpg
		$active_available_size_objects = $this->getShoeSizes();

		if(count($active_available_size_objects) == 0) {
			logm('All sold out of size we want.');
			// todo: stop script & send signal
		}

		$this->addToCart($active_available_size_objects);

		$raffle_rules_okay_btn = $this->helpers->waitUntilShown($this->driver, '.checkout-body .cta-btn', 'css', 1000, 3);
		if($raffle_rules_okay_btn) { // todo: test this
			logm('Clicking on raffle okay button');
			$raffle_rules_okay_btn->click();
		}

		while(!$this->paymentBoxShown()) {
			// payment-component
			logm('Payment box not showing. Sleeping for 5s and then rechecking.');

			sleep(5);
		}

		$this->clickOnPaymentMethod();

		sleep(2);

		$this->enterCCV();

		sleep(2);

		$this->clickFinalCheckout();

		sleep(2);

		$this->waitForW();

		logm('zen');
    }

    public function addToCart($active_available_size_objects)
	{
		$selected_shoe_size = $active_available_size_objects[rand(0, count($active_available_size_objects) - 1)];
		logm('Adding size "' . $selected_shoe_size->getText() . '" to cart');
		$this->driver->executeScript('arguments[0].scrollIntoView(true);', [$selected_shoe_size]);
		$this->driver->executeScript("window.scrollBy(0,-200)");

		sleep(2);

		$selected_shoe_size->click();

		sleep(2);

		logm('Clicking add to cart button');

		$add_to_cart_button = $this->helpers->waitUntilShown($this->driver, '[data-qa="feed-buy-cta"]', 'css', 1000, 3);
		if($add_to_cart_button) { // todo: test this if block
			logm('Non-raffle checkout');

			logm('Scrolling to add to cart button');
			$this->driver->executeScript('arguments[0].scrollIntoView(true);', [$add_to_cart_button]);
			$this->driver->executeScript("window.scrollBy(0,200)");

			sleep(4);

			$add_to_cart_button->click();
		} else {
			logm('Raffle checkout?');

			$raffle_add_to_cart = $this->driver->findElement(WebDriverBy::cssSelector('aside.product-info-container .ncss-btn-primary-dark'));
			if ($raffle_add_to_cart) {
				logm('Scrolling to raffle add to cart button');
				$this->driver->executeScript('arguments[0].scrollIntoView(true);', [ $raffle_add_to_cart ]);
				$this->driver->executeScript("window.scrollBy(0,-200)"); // todo: does -200 work?

				sleep(4);

				logm('Clicking raffle add to cart');
				$raffle_add_to_cart->click();
			}
		}
	}

    public function getShoeSizes()
	{
		$active_available_size_objects = [];

		$this->helpers->waitUntilShown($this->driver, '.card-product-component [data-qa="size-available"]', 'css', 3000);

		// .card-product-component forces non-youth sizes for future update.
		$available_size_objects = $this->driver->findElements(WebDriverBy::cssSelector('.card-product-component [data-qa="size-available"]'));
		foreach($available_size_objects as $available_size_object) {
			$available_size = $available_size_object->getText();

			if(strpos(strtolower($available_size), 'y') === false && strpos(strtolower($available_size), 'c') === false) {
				preg_match_all("/((\d*\.?\d+)) |^(\d*\.?\d+)/", $available_size, $matches);
				$shoe_size_parsed = trim($matches[0][0]);

				if (in_array($shoe_size_parsed, $this->other['sizes'])) {
					logm($shoe_size_parsed . ' SIZE AVAILABLE');
					$active_available_size_objects [] = $available_size_object;
					// todo: send back available sizes to server.
					// todo this is kind of slow?
				}
			} else {
				logm('Skipping child size: ' . $available_size); // todo this never prints when it's enabled.
			}
		}

		return $active_available_size_objects;
	}

    public function hasDropped()
	{
		// $unworkable_button = $this->driver->findElements(WebDriverBy::className('buttoncount-1'));
		// class: buttoncount-1 // for when div.gettext = Sold Out or notify me // todo:

		$sizes = $this->driver->findElements(WebDriverBy::cssSelector('[data-qa="size-available"]'));
		if(count($sizes) > 0) {
			// todo: send live signal to server.

			return true;
		}

		$check_live = file_get_contents('https://www.easyhuzl.com/api/check-live/'.$this->other['drop_id'].'/');
		$check_live_json = json_decode($check_live, true);
		if($check_live_json['is_live'] && !$this->has_reloaded) {
			logm('Refreshing because it is live and we don\'t see sizes.');

			$this->has_reloaded = true;
			$this->driver->navigate()->refresh();
		} else if(!$check_live_json['is_live'] && $this->has_reloaded) {
			logm('Resetting has reloaded flag to prepare for another refresh.');

			$this->has_reloaded = false;
		} else if(!empty($check_live_json['msg'])) {
			logm('Remote msg: ' . $check_live_json['msg']);
		}

		return false;
	}

	public function paymentBoxShown()
	{
		$payment_box = $this->driver->findElements(WebDriverBy::cssSelector('.payment-component > div'));

		return count($payment_box) > 0;
	}

	public function clickOnPaymentMethod()
	{
		logm('Clicking on payment method');

		$payment_method = $this->helpers->waitUntilShown($this->driver, '.payment-section .ncss-label', 'css', 3000);
		if($payment_method) {
			$payment_method->click();
		}
	}

	public function enterCCV()
	{
		logm('Waiting for CCV (' . $this->account['ccv'] . ') frame.');

		$ccv_frame = $this->helpers->waitUntilShown($this->driver, '[title="creditCardIframeForm"]', 'css', '2000', 3);
		if($ccv_frame) {
			logm('Switching frames');
			$this->driver->switchTo()->frame($ccv_frame); // todo: this doesnt always work?

			sleep(4);

			$ccv_element = $this->helpers->waitUntilShown($this->driver, '#cvNumber', 'css', '2000', 3); // todo: it may be this being the issue
			if($ccv_element) {
				logm('Entering CCV (' . $this->account['ccv'] . ')');
				$ccv_element->sendKeys($this->account['ccv']);

				sleep(3);
			} else {
				logm('Unable to find ccv element... oops?');
				$ccv_element = $this->helpers->waitUntilShown($this->driver, '#number', 'css', '2000', 3);

				if($ccv_element) {
					logm('Able to find #number');
				} else {
					logm('unable to find #number');
				}
			}

			$this->driver->switchTo()->parent();
		} else {
			logm('No CCV field found, moving on hopefully.');
			sleep(2);
		}

		$save_button = $this->helpers->waitUntilShown($this->driver, '.payment-section [data-qa="save-button"]', 'css', '2000');

		logm('Clicking on next button.');
		if($save_button) {
			$save_button->click();
		}
	}

	public function clickFinalCheckout()
	{
		logm('Clicking final checkout button');
		$final_checkout_button = $this->helpers->waitUntilShown($this->driver, '.checkout-summary-section-content .ncss-btn-primary-dark', 'css', 3000);
		if($final_checkout_button) {
			$final_checkout_button->click();
		}

		$are_you_sure_button = $this->helpers->waitUntilShown($this->driver, '[data-qa="presubmit-confirm"]', 'css', 2000, 3);
		if($are_you_sure_button) { // todo: test
			logm('Clicking raffle are you sure continue button.');
			$are_you_sure_button->click();
		}
	}

	public function waitForW()
	{
		logm('Waiting for W...');

		$summary_content = $this->helpers->waitUntilShown($this->driver, '.checkout-summary-section-content', 'css', '2000', 5);
		if($summary_content) {
			logm('Oh no there was an error!');

			logm($summary_content->getText());
			// todo: submit error
		}
		// .checkout-summary-section-content
		// if there's an error

		$got_em = $this->helpers->waitUntilShown($this->driver, '.completed-layout .headline-3', 'css', '5000', 30);
		if($got_em && $got_em->getText() == "Got 'em") { // todo: test
			logm('Got a w!!!');
			// todo: alert server
		} else if($got_em) {
			logm('Joined?');
			logm($got_em->getText()); // Didn't get 'em

			// todo: test below this

			if($got_em->getText() == 'Joined') {
				logm('We are joined, going to wait for a status update...');
				logm('current status: ' . $got_em->getText());

				while($got_em->getText() == 'Joined') {
					logm('Text still Joined, waiting');
					sleep(20);
				}
			}

			if($got_em->getText() == "Got 'em") {
				logm('Got a w!!!');
			} else if($got_em->getText() == "Didn't get 'em") {
				logm('didnt get them!!!');
			}
		}


		// todo: damn didn't get em ??
	}
}
