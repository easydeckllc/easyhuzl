<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Controllers\Buy;

require_once('Controllers/Buy.php');
require_once('Controllers/Login.php');
require_once('Helpers.php');
require_once('Log.php');

class Runner
{
    private $start_time;
    private $server_type;

    /** @var RemoteWebDriver */
    private $driver;
	private $log;
	private $helpers;

	private $account;
	private $other;

    function __construct($action, $account, $other)
    {
        $this->start_time = time();

		$this->helpers = new Helpers();

        $this->action = $action;
        $this->account = $account;
        $this->other = $other;
    }

    public function main()
    {
    	if(file_exists('../server_type.txt')) {
			$this->server_type = file_get_contents('../server_type.txt');
		} else if(file_exists('server_type.txt')) {
			$this->server_type = file_get_contents('server_type.txt');
		} else {
			$this->server_type = 'linux';
		}

		try {
			$this->boot($this->account['proxy_info']);
		} catch (WebDriverCurlException $e) {
    		logm('Exception.');
		} catch (exception $e) {
			logm('Booting exception :(');
			print_r($e);
		}

        if($this->action == 'buy') {
			$buy_obj = new Buy($this->driver, $this->log, $this->helpers, $this->account, $this->other);
			$buy_obj->main();
		} else {
			logm('Action (' . $this->action . ') not implemented yet!');
		}

		logm("Time ran: " . (time() - $this->start_time));

        // todo: send goodbye signal with time ran
    }

    private function boot($proxy)
    {
		logm("Booting up " . $this->server_type);

        $options = new ChromeOptions();
        $capabilities = DesiredCapabilities::chrome();

        $option_arguments = [
			'allow-running-insecure-content',
			'disable-web-security',
			'window-size=1300,1024',
		];

        if($this->server_type == 'macos') {
			$option_arguments []= 'user-data-dir=/Users/traceremick/PhpstormProjects/easyhuzl/chrome_datadir';
			// $option_arguments []= 'load-extension=./easyhuzl-proxy';
		} else {
			$this->replaceAuthProxyExtension($proxy['username'], $proxy['password']);

			$option_arguments []= 'disable-dev-shm-usage';
			$option_arguments []= 'no-sandbox';
			$option_arguments []= 'user-data-dir=/root/Desktop/client/chrome_datadir';
			$option_arguments []= 'load-extension=/root/Desktop/client/easyhuzl-proxy';
			// todo: https://stackoverflow.com/questions/51569562/while-executing-selenium-script-in-chrome-browser-getting-failed-to-load-extensi

			if (!empty($proxy['ip']) && !empty($proxy['port'])) {
				$option_arguments []= 'proxy-server='.$proxy['ip'] . ':' . $proxy['port'];
			}
		}

		$options->addArguments($option_arguments);

        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);

        $this->driver = RemoteWebDriver::create('http://localhost:4444', $capabilities, 60000, 60000);

		if($this->server_type !== 'macos') {
			$this->driver->manage()->window()->maximize();
		}

		logm("End Booting");
    }

    public function replaceAuthProxyExtension($username, $password)
	{
		$extension_background = file_get_contents('./easyhuzl-proxy/background.js');

		$username_replacement = preg_replace('/(username = ")(.*)(";)/', 'username = "'. $username .'";', $extension_background);
		$password_replacement = preg_replace('/(password = ")(.*)(";)/', 'password = "'.$password.'";', $username_replacement);

		file_put_contents('./easyhuzl-proxy/background.js', $password_replacement);

		exec('cd /root/Desktop/client/easyhuzl-proxy && zip ../easyhuzl-proxy.zip * && cd ../');

		sleep(2);
	}
}
