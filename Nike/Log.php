<?php
function logm($message)
{
	$log = "[" . date('n/j/Y g:i:s A', time() - (3600 * 5)) . "] " . $message. "\n";

	echo $log;

	$post = ['message' => $log];

	// todo: do this every couple of messages, maybe every 5 or end?
	$ch = curl_init('https://www.easyhuzl.com/api/log');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	// execute!
	$response = curl_exec($ch);

	// close the connection, release resources used
	curl_close($ch);
}
